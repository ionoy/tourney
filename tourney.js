﻿Tourney = {
    render: function (displayEl, contentEl) {
        displayEl.html(contentEl.html());        
        this.update(displayEl);
    },
    update: function(el) {
        $("group", el).each(function(ei, e) {
            Tourney.renderGroup($(e));
        });
        $("bracket", el).each(function(ei, e) {
            Tourney.renderBracket($(e));
        });
    },
    renderGroup: function($e) {
        var text=$e.html();
        var matchesRegex=/<matches>([\s\S]+)<\/matches>/;
        var matchesMatch=text.match(matchesRegex);
        var matches=null;
        if(matchesMatch) {
            text=text.replace(matchesRegex,"");
            matches=Tourney.renderMatches(matchesMatch[1]);
        }

        var resultTag=$("<table/>").addClass("group");
        var row="<tr></tr>";
        var splitted=Tourney.getLines(text);

        for(var i=0;i<splitted.length;i++) {
            var str=splitted[i];
            if(i==0) {
                resultTag.append($(row).append($("<th></th").text(str).attr("colspan","8")));
            } else {
                var newRow=$(row).append(Tourney.matchFormat(str));

                if(str.indexOf(" w")==str.length-2)
                    newRow.addClass("w");
                if(str.indexOf(" l")==str.length-2)
                    newRow.addClass("l");

                resultTag.append(newRow);
            }
        }

        if(!!matches) {
            var matchesTable=$("<table class='matches-table'/>").append(matches.hide());
            var tfoot=$("<tfoot/>").appendTo(resultTag);
            var showButton=$("<tr><td colspan='8'><a href='#'>Show matches</a></td></tr>").find("td").append(matchesTable);
            tfoot.prepend(showButton).click(function() {
                tfoot.find("tr").toggle();
                var a=$("a",this);
                a.text(a.text()=="Show matches"?"Hide matches":"Show matches");
                return false;
            });
        }

        $e.replaceWith(resultTag);
    },
    renderBracket: function($e) {
        var lines = this.getLines($e.text());
        var stages = this.getStages(lines);
        var resultTag = $("<div class='bracket'/>");
        var matchHeight = 50;
        var stageWidth = 150;
        var isDoubleElimination = $e[0].hasAttribute("double");
        var canvas;

        resultTag.width(stages.length * stageWidth);
        for(var s in stages) {
            var stage = stages[s];
            if(s==0) {
                resultTag.height(stage.games.length * matchHeight * 2);
                canvas = $("<canvas/>");
                resultTag.append(canvas);
                canvas[0].width = resultTag.width();
                canvas[0].height = resultTag.height();
            }

            var stageNameEl = $("<div class='stage-name'/>").text(stage.name);
            stageNameEl.offset({ top: 0, left: s * stageWidth }).width(stageWidth);
            resultTag.append(stageNameEl);
            
            var matchVerticalOffset = resultTag.height() / (stage.games.length + 1);

            var pretty = function(str) {
                if (!!str)
                    return str;
                return "";
            };
            
            for(var m in stage.games) {
                var match = stage.games[m];
                var matchEl = $("<div class='match' />");
                var top = isDoubleElimination ? m * matchVerticalOffset + matchVerticalOffset : m * matchVerticalOffset + Math.pow(2, s) * matchHeight;                
                var content = "<span class='player'>" + pretty(match.p1) + "</span>" +
                              "<span class='score'><div>" + pretty(match.p1s) + "</div><div>" + pretty(match.p2s) + "</div></span>" +
                              "<span class='player'>" + pretty(match.p2) + "</span>";

                matchEl.offset({ top: top, left: s * stageWidth });
                match.matchEl = matchEl;
                
                if(match.p1link === undefined && match.p2link === undefined) {
                    match.p1link = m * 2;
                    match.p2link = m * 2 + 1;
                }
                
                resultTag.append(matchEl.append(content));
            }
        }
        
        $e.replaceWith(resultTag);
        for (var ss in stages) {
            var st = stages[ss];
            if (ss > 0) {
                for (var mm in st.games) {
                    Tourney.linkMatch(canvas[0], st.games[mm], stages[ss - 1]);
                }
            }
        }
    },
    renderMatches: function(str) {
        var lines = Tourney.getLines(str);
        var result = "";
        for(var l in lines) {
            var line = lines[l];
            var match = line.match(/(.+)\s(\d+)\:(\d+)\s(.+)/);
            if(match) {
                result += "<tr><td class='player'>" + match[1] + "</td><td class='score'>" + match[2] + ":" + match[3] + "</td><td class='player'>"+ match[4] +"</td></tr>";
            }
        }
        return $(result);
    },
    linkMatch: function(canvas,match,prevStage) {
        var height = match.matchEl.height();
        var width = match.matchEl.width();
        var drawLink = function(link, off) {
            var pos = match.matchEl.position();
            if((!!link || link === 0) && link < prevStage.games.length) {                
                var dest = prevStage.games[link].matchEl.position();
                Tourney.drawLine(canvas, pos.left, pos.top + off, dest.left + width, dest.top + height / 2);                
            } else {
                Tourney.drawLine(canvas, pos.left, pos.top + off, pos.left - 5, pos.top + off);
                Tourney.drawLine(canvas, pos.left - 5, pos.top + off, pos.left - 5, pos.top + off - 5);
            }
        };
        drawLink(match.p1link, height / 4);
        drawLink(match.p2link, height / 4 * 3);
    },
    getStages: function(lines) {
        var stages = [];
        var currentStage = { name: "", games: [] };
        for(var l in lines) {
            var s = lines[l];
            var stageMatch = s.match( /\-(.*)/ );
            if(stageMatch) {
                if(l > 0) {
                    stages.push(currentStage);
                    currentStage = { name: stageMatch[1], games: [] };
                } else {
                    currentStage.name = stageMatch[1];
                }
            } else {
                var matchMatch = s.match(/(.+)?\s*(vs|(\d+):(\d+))\s*([^\(]+)?\s*(\((\d+)?\s(\d+)?\))?/ );
                if(matchMatch) {
                    currentStage.games.push({
                        p1: matchMatch[1],
                        p2: matchMatch[5],
                        p1s: matchMatch[3],
                        p2s: matchMatch[4],
                        p1link: matchMatch[7],
                        p2link: matchMatch[8]
                    });
                }
            }
        }
        stages.push(currentStage);
        return stages;
    },
    getLines: function(str) {
        var splitted = str.split("\n");
        var result = [];
        for(var k in splitted) {
            var s = splitted[k].trim();
            if(s.length != 0)
                result.push(s);
        }
        return result;
    }, 
    matchFormat: function(str) {
        var formats = Tourney.formats;
        for(var f in formats) {
            var match = str.match(formats[f].regex);
            if(match) {
                return formats[f].render(match);
            }
        }
        return "";
    },
    drawLine: function(canvas, x1, y1, x2, y2) {
        var ctx = canvas.getContext('2d');
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
    },
    formats: [{
        regex: /(.+):\s(\d+)\s(\d+)\s(\d+)\s(\d+)\s(\d+)/,
        render: function(match) {
            var result = "<td class='player-name'>" + match[1] + "</td>";
            result += "<td class='score'>" + match[2] + ":" + match[3] + "</td>";
            result += "<td class='score'>" + match[4] + ":" + match[5] + "</td>";
            result += "<td class='score'>" + match[6] + "</td>";

            return $(result);
        }
    },
    {
        regex: /(.+):\s(\d+)\s(\d+)\s(\d+)\s(\d+)/,
        render: function(match) {
            var result="<td class='player-name'>"+match[1]+"</td>";
            result+="<td class='score'>"+match[2]+":"+match[3]+"</td>";
            result+="<td class='score'>"+match[4]+":"+match[5]+"</td>";

            return $(result);
        }
    },
    {
        regex: /(.+):\s(\d+)\s(\d+)\s(\d+)/,
        render: function(match) {
            var result="<td class='player-name'>"+match[1]+"</td>";
            result+="<td class='score'>"+match[2]+":"+match[3]+"</td>";
            result+="<td class='score'>"+match[4]+"</td>";

            return $(result);
        }
    },
    {
        regex: /(.+):\s(\d+)\s(\d+)/,
        render: function(match) {
            var result="<td class='player-name'>"+match[1]+"</td>";
            result+="<td class='score'>"+match[2]+":"+match[3]+"</td>";

            return $(result);
        }
    }]
}